package com.sofy.chat.filters;

import com.sofy.chat.service.JwtService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Component
@AllArgsConstructor
public class JwtFilter extends GenericFilterBean {

    private JwtService jwtService;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        //get token from Http request
        final String token = jwtService.resolveToken((HttpServletRequest) servletRequest);
        //validate token. Check secret key and expiration time
        if (token != null && jwtService.validateToken(token)) {
            //create authentication object
            final Authentication authentication = jwtService.getAuthentication(token);
            if (authentication != null) {
                //put authentication object to the Security Context
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
