package com.sofy.chat.controllers;

import com.sofy.chat.dto.MessageDto;
import com.sofy.chat.dto.MessageRequest;
import com.sofy.chat.dto.RegistrationDto;
import com.sofy.chat.dto.UserDto;
import com.sofy.chat.service.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.security.Principal;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/v1/chats")
public class MessageController {
    private final MessageService messageService;

    @GetMapping()
    public ResponseEntity<List<UserDto>> findActiveChats(@NotEmpty Principal principal) {
        return ResponseEntity.ok(messageService.findActiveChats(principal));
    }

    @GetMapping(params = {"email"})
    public ResponseEntity<List<UserDto>> findChats(@NotEmpty Principal principal, @RequestParam String email) {
        return ResponseEntity.ok(messageService.findChats(principal, email));
    }

    @GetMapping(value = "/{otherUserId}")
    public ResponseEntity<List<MessageDto>> findMessages(@NotEmpty Principal principal, @PathVariable long otherUserId) {
        return ResponseEntity.ok(messageService.findMessages(principal, otherUserId));
    }

    @PostMapping(value = "/{otherUserId}")
    public ResponseEntity<Void> saveMessage(@NotEmpty Principal principal, @PathVariable long otherUserId, @Valid @RequestBody MessageRequest request) {
        messageService.saveMessages(principal, otherUserId, request);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
