package com.sofy.chat.controllers;

import com.sofy.chat.dto.LoginDto;
import com.sofy.chat.dto.RegistrationDto;
import com.sofy.chat.dto.TokenDto;
import com.sofy.chat.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/v1/auth")
public class AuthController {
    private final UserService userService;

    @PostMapping(value = "/register")
    public ResponseEntity<Void> register(@Valid @RequestBody RegistrationDto request) {
        userService.register(request);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "/login")
    public ResponseEntity<TokenDto> login(@RequestBody @Valid LoginDto request) {
        String token = userService.createToken(request);
        return ResponseEntity.ok(new TokenDto(token));
    }
}
