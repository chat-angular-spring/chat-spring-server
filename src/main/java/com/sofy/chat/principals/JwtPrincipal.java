package com.sofy.chat.principals;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.security.auth.Subject;
import java.security.Principal;
import java.util.Collection;

import static java.util.stream.Collectors.toList;

public class JwtPrincipal implements Principal {
    private String username;
    private Collection<? extends GrantedAuthority> authorities;

    public JwtPrincipal(final String username, final Collection authorities){
        this.username = username;
        this.authorities = toAuthorities(authorities);
    }

    private Collection<? extends GrantedAuthority> toAuthorities(final Collection<Object> authorities) {
        return authorities.stream()
                .filter(authority -> authority instanceof String)
                .map(role -> new SimpleGrantedAuthority((String) role))
                .collect(toList());
    }

    @Override
    public String getName() {
        return username;
    }

    @Override
    public boolean implies(Subject subject) {
        return false;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }
}

