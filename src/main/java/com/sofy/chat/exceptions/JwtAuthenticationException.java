package com.sofy.chat.exceptions;

import org.springframework.security.core.AuthenticationException;

public class JwtAuthenticationException extends AuthenticationException {
    public JwtAuthenticationException(final String message) {
        super(message);
    }
}
