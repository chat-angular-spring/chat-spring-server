package com.sofy.chat.entity.message;

import com.sofy.chat.entity.BaseEntity;
import com.sofy.chat.entity.user.UserEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "message")
@Data
public class MessageEntity extends BaseEntity {
    @ManyToOne
    private UserEntity from;
    @ManyToOne
    private UserEntity to;
    private Date date = new Date();
    private String message;
}
