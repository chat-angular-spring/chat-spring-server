package com.sofy.chat.dto;

import lombok.Data;

@Data
public class MessageDto {
    private String text;
    private Boolean isMe = false;
}
