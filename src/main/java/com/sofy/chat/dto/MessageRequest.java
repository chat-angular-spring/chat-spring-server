package com.sofy.chat.dto;

import lombok.Data;

@Data
public class MessageRequest {
    private String text;
}
