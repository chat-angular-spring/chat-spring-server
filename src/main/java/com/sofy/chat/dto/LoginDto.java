package com.sofy.chat.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class LoginDto {
    @NotEmpty
    @Email(message = "Invalid email")
    @Size(max = 30, message = "The email must be shorter than {max}.")
    private String email;

    @NotEmpty
    @Pattern(regexp = "(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[A-Za-z0-9]{8,16}", message = "the password must be entered a letter and numbers and be longer than 8 characters and shorter than 16.")
    private String password;
}
