package com.sofy.chat.dto;

import lombok.Data;

@Data
public class UserDto {
    private long id;
    private String email;
}
