package com.sofy.chat.repository;

import com.sofy.chat.entity.message.MessageEntity;
import com.sofy.chat.entity.user.UserEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MessageRepository extends CrudRepository<MessageEntity, Long> {
    @Query(nativeQuery = true, value =
            "select * from (select * from message as m1 where m1.to_id = :user1 and m1.from_id = :user2 " +
            "union select * from message as m2 where m2.to_id = :user2 and m2.from_id = :user1) as m3 order by m3.date")
    List<MessageEntity> findAllMessages(long user1, long user2);
}
