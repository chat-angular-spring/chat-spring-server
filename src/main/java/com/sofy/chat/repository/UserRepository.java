package com.sofy.chat.repository;

import com.sofy.chat.entity.user.UserEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<UserEntity, Long> {

    Optional<UserEntity> findByEmail(String email);

    @Query(nativeQuery = true, value =
            "select * from user u where u.id != :user and u.email like :email")
    List<UserEntity> findChats(long user, String email);

    @Query(nativeQuery = true, value =
            "select * from user u where u.id in (select c.user_id from (select m3.user_id, m3.write_date from " +
                    "(select m1.from_id as user_id, m1.date as write_date from message as m1 where m1.to_id = :user " +
                    "union select m2.to_id as user_id, m2.date as write_date from message as m2 where m2.from_id = :user)" +
                    "as m3 order by m3.write_date) as c group by user_id)")
    List<UserEntity> findAllChats(long user);

}
