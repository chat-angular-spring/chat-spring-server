package com.sofy.chat.service;

import com.sofy.chat.entity.user.RoleEntity;
import com.sofy.chat.entity.user.UserEntity;
import com.sofy.chat.exceptions.JwtAuthenticationException;
import com.sofy.chat.principals.JwtPrincipal;
import com.sofy.chat.tokens.JwtAuthenticationToken;
import io.jsonwebtoken.*;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class JwtService {

    private String secret = "wvuqaQJV0bTB7Z2b";

    private long validityInMilliseconds = 360000000;

    public String resolveToken(final HttpServletRequest req) {
        final String bearerToken = req.getHeader("Authorization");
        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7);
        }
        return null;
    }

    public boolean validateToken(final String token) {
        try {
            final Jws<Claims> claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token);
            return !claims.getBody().getExpiration().before(new Date());
        } catch (JwtException | IllegalArgumentException e) {
            throw new JwtAuthenticationException("JWT token is expired or invalid");
        }
    }

    public Authentication getAuthentication(final String token) {
        final Jws<Claims> claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token);
        final JwtPrincipal principal = new JwtPrincipal(claims.getBody().get("sub", String.class),
                claims.getBody().get("roles", List.class));

        return new JwtAuthenticationToken(principal);
    }

    public String createToken(UserEntity user) {
        final Claims claims = Jwts.claims().setSubject(user.getUsername());
        claims.put("roles", getRoleNames(user.getRoles()));

        final Date now = new Date();
        final Date validity = new Date(new Date().getTime() + validityInMilliseconds);

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();
    }

    private List<String> getRoleNames(final List<RoleEntity> roles) {
        return roles.stream().map(RoleEntity::name).collect(Collectors.toList());
    }

}
