package com.sofy.chat.service;

import com.sofy.chat.dto.LoginDto;
import com.sofy.chat.dto.RegistrationDto;
import com.sofy.chat.entity.user.UserEntity;
import com.sofy.chat.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {
    private final UserRepository userRepository;
    private final JwtService jwtService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByEmail(username).orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }

    public String createToken(LoginDto dto) {
        final UserEntity user = userRepository.findByEmail(dto.getEmail()).orElseThrow(() ->
                new UsernameNotFoundException(String.format("User with username: %s not found", dto.getEmail())));

        if (!bCryptPasswordEncoder.matches(dto.getPassword(), user.getPassword())){
            throw new UsernameNotFoundException(String.format("User with username: %s not found", dto.getEmail()));
        }

        return jwtService.createToken(user);
    }

    public void register(RegistrationDto dto) {
        UserEntity entity = new UserEntity();
        entity.setEmail(dto.getEmail());
        entity.setPassword(bCryptPasswordEncoder.encode(dto.getPassword()));
        userRepository.save(entity);
    }
}
