package com.sofy.chat.service;

import com.sofy.chat.dto.MessageDto;
import com.sofy.chat.dto.MessageRequest;
import com.sofy.chat.dto.UserDto;
import com.sofy.chat.entity.message.MessageEntity;
import com.sofy.chat.entity.user.UserEntity;
import com.sofy.chat.exceptions.BadRequestException;
import com.sofy.chat.exceptions.JwtAuthenticationException;
import com.sofy.chat.repository.MessageRepository;
import com.sofy.chat.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class MessageService {
    private final MessageRepository messageRepository;
    private final UserRepository userRepository;

    public List<UserDto> findChats(Principal principal, String email) {
        final UserEntity user = userRepository.findByEmail(principal.getName()).orElseThrow(() ->
                new JwtAuthenticationException(String.format("Not found user by username: %s", principal.getName())));

        String pattern = '%' + email + '%';

        List<UserEntity> users = userRepository.findChats(user.getId(), pattern);

        List<UserDto> chats = users.stream().map(this::toDto).toList();

        return chats;
    }

    public List<UserDto> findActiveChats(Principal principal) {
        final UserEntity user = userRepository.findByEmail(principal.getName()).orElseThrow(() ->
                new JwtAuthenticationException(String.format("Not found user by username: %s", principal.getName())));

        List<UserEntity> users = userRepository.findAllChats(user.getId());

        List<UserDto> chats = users.stream().map(this::toDto).toList();

        return chats;
    }

    public List<MessageDto> findMessages(Principal principal, long otherUser) {
        final UserEntity user = userRepository.findByEmail(principal.getName()).orElseThrow(() ->
                new JwtAuthenticationException(String.format("Not found user by username: %s", principal.getName())));

        List<MessageEntity> messagesEntity = messageRepository.findAllMessages(user.getId(), otherUser);

        List<MessageDto> messages = messagesEntity.stream().map(message -> toMessageDto(message, user)).toList();

        return messages;
    }

    public void saveMessages(Principal principal, long otherUser, MessageRequest request) {
        final UserEntity userFrom = userRepository.findByEmail(principal.getName()).orElseThrow(() ->
                new JwtAuthenticationException(String.format("Not found user by username: %s", principal.getName())));

        final UserEntity userTo = userRepository.findById(otherUser).orElseThrow(() ->
                new BadRequestException("Not found user"));

        MessageEntity entity = new MessageEntity();
        entity.setMessage(request.getText());
        entity.setFrom(userFrom);
        entity.setTo(userTo);

        messageRepository.save(entity);
    }

    private UserDto toDto(UserEntity entity){
        UserDto userDto = new UserDto();
        userDto.setId(entity.getId());
        userDto.setEmail(entity.getEmail());
        return userDto;
    }

    private MessageDto toMessageDto(MessageEntity entity, UserEntity user){
        MessageDto messageDto = new MessageDto();
        messageDto.setText(entity.getMessage());
        messageDto.setIsMe(entity.getFrom().getId() == user.getId());
        return messageDto;
    }
}
